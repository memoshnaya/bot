module gitlab.com/memoshnaya/bot

go 1.14

require (
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.6.0
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa
	google.golang.org/appengine v1.6.6 // indirect
)
