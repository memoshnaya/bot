package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	_ "github.com/lib/pq"
)

func main() {
	log.Println("starting")

	//dbHost := os.Getenv("DATABASE_HOST")
	//dbUser := os.Getenv("DATABASE_USER")
	//dbName := os.Getenv("DATABASE_NAME")
	//dbPass := os.Getenv("DATABASE_PASSWORD")

	//db, err := sqlx.Connect("postgres",
	//	fmt.Sprintf(
	//		"host=%s password=%s user=%s dbname=%s sslmode=disable",
	//		dbHost, dbPass, dbUser, dbName,
	//	))
	//if err != nil {
	//	log.Fatalf("could not connect to pg: %v", err)
	//}
	//
	//a := 0
	//if err := db.QueryRow("SELECT 1").Scan(&a); err != nil {
	//	log.Fatalf("count a:%v", err)
	//}
	//
	//log.Println(a)

	token := os.Getenv("BOT_TOKEN")
	if token == "" {
		log.Fatal("could not get token from env (BOT_TOKEN)")
	}

	var (
		bot     *tgbotapi.BotAPI
		updates tgbotapi.UpdatesChannel
		err     error
	)
	botMode := os.Getenv("BOT_MODE")
	if botMode == "webhook" {
		bot, err = tgbotapi.NewBotAPI(token)
		if err != nil {
			log.Fatalf("could not create bot: %v", err)
		}
		bot.Debug = true
		log.Printf("Authorized on account %s", bot.Self.UserName)

		srvAddr := os.Getenv("SRV_ADDR")
		if srvAddr == "" {
			log.Fatal("server addr not set")
		}

		_, err = bot.SetWebhook(tgbotapi.NewWebhookWithCert(fmt.Sprintf("https://%s/%s", srvAddr, bot.Token), "/etc/certs/bot.pem"))
		if err != nil {
			log.Fatalf("could not set webhook: %v", err)
		}

		info, err := bot.GetWebhookInfo()
		if err != nil {
			log.Fatalf("could not get webhook info: %v", err)
		}
		if info.LastErrorDate != 0 {
			log.Printf("Telegram callback failed: %s", info.LastErrorMessage)
		}

		updates = bot.ListenForWebhook("/" + bot.Token)
		go func() {
			err = http.ListenAndServe(":8000", nil)
			if err != nil {
				log.Fatalf("could not listen 8000: %v", err)
			}
		}()
	} else {
		bot, err = tgbotapi.NewBotAPI(token)
		if err != nil {
			log.Fatalf("could not create bot api: %v", err)
		}

		res, err := bot.RemoveWebhook()
		if err != nil {
			log.Fatalf("could not remove webhook: %v", err)
		}

		log.Println(res)

		u := tgbotapi.NewUpdate(0)
		u.Timeout = 60
		updates, err = bot.GetUpdatesChan(u)
		if err != nil {
			log.Fatalf("could not get u chan: %v", err)
		}
	}

	for u := range updates {
		if u.Message == nil { // ignore any non-Message Updates
			continue
		}

		log.Printf("[%s] %s", u.Message.From.UserName, u.Message.Text)

		msg := tgbotapi.NewMessage(u.Message.Chat.ID, u.Message.Text)
		msg.ReplyToMessageID = u.Message.MessageID

		if _, err := bot.Send(msg); err != nil {
			log.Fatal(err)
		}
	}
}
